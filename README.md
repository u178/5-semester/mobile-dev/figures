## Abstract Figure class. Inheritance from it and Transforming interface



### Figure class methods:

1. **area()** -> get_area

2. **print()** -> describe figure

### Figures:

1. **Rect**

2. Square

3. Circle

## Interface Movable methods:

1. **move(dx, dy)** -> move figure

## Interface Transforing methods:

1. **resize(zoom)** -> resize figure

2. **rotate(direction, centerX, centerY)** -> rotate Clockwise or CounterClockwise