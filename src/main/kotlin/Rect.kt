class Rect
    (
        private var width: Int,
        private var height: Int,
        private var x: Int,
        private var y: Int,
    ) : Movable, Transforming, Figure(0) {


//    constructor(rect: Rect) : this(rect.width, rect.height, rect.x, rect.y)

    override fun move(dx: Int, dy: Int) {
        x += dx
        y += dy
    }

    override fun print() {
        println("rect (x: $x, y: $y, height: $height, width: $width)")
    }

    override fun area(): Float {
        return (width * height).toFloat()
    }

    override fun resize(zoom: Int) {
        width *= zoom
        height *= zoom
    }

    override fun rotate(direction: RotateDirection, centerX: Int, centerY: Int) {
        val dx = centerX - x // 1
        val dy = centerY - y // -1
        if (direction == RotateDirection.Clockwise) {
            x = centerX - dy
            y = centerY + dx
        }
        if (direction == RotateDirection.CounterClockwise) {
            x = centerX + dx
            y = centerY - dy
        }
    }
}