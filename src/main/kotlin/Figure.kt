abstract class Figure(val id: Int) {
    abstract fun print()
    abstract fun area() : Float
}