class Square(
    private var size: Int,
    private var x: Int,
    private var y: Int,
) : Movable, Transforming, Figure(0) {


    override fun print() {
        println("rect (x: $x, y: $y, size: $size)")
    }

    override fun area(): Float {
        return (size * size).toFloat()
    }

    override fun move(dx: Int, dy: Int) {
        x += dx
        y += dy
    }

    override fun resize(zoom: Int) {
        size *= zoom
    }

    override fun rotate(direction: RotateDirection, centerX: Int, centerY: Int) {
        val dx = centerX - x // 1
        val dy = centerY - y // -1
        if (direction == RotateDirection.Clockwise) {
            x = centerX - dy
            y = centerY + dx
        }
        if (direction == RotateDirection.CounterClockwise) {
            x = centerX + dx
            y = centerY - dy
        }
    }
}